package main

import "fmt"

func main() {
	fmt.Println(greet("Jane ", "Doe"))
}

func greet(fname, lname string) (namedReturnType string) {
	namedReturnType = fmt.Sprint(fname, lname)
	return
}
