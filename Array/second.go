package main

import "fmt"

func main() {

	arr := [5]string{"India", "China", "Nepal", "USA", "New Zealand"}

	fmt.Println("\n---------------Example 1--------------------\n")
	for i := 0; i < len(arr); i++ {
		fmt.Println(arr[i])
	}
}
