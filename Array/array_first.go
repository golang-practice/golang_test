package main

import "fmt"

func main() {
	var intArray [5]int
	strArray := [2]string{"good", "great"}
	floatArray := [2]float64{1.2, 9.9}
	boolArray := [2]bool{true, true}
	elipsisArray := [...]int{1, 9, 8, 7}

	fmt.Println(intArray)
	fmt.Println(strArray)
	fmt.Println(floatArray)
	fmt.Println(boolArray)
	fmt.Println(elipsisArray)

}
