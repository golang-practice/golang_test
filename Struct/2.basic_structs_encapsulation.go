package main

import "fmt"

type person struct {
	first string
	last  string
	age   int
}

func main() {
	p1 := person{"James", "Bond", 20}
	fmt.Println(p1)
	p2 := person{first: "James", last: "Bond", age: 45}
	fmt.Println(p2)
	p3 := person{first: "James", age: 45}
	fmt.Println(p3)
}
