package main

import "fmt"

type foo int

func main() {
	var myAge foo
	myAge = 44
	fmt.Printf("This persons age is %v and its type is %T \n", myAge, myAge)

	var yourAge int
	yourAge = 29
	fmt.Printf("This persons age is %v and its type is %T \n", yourAge, yourAge)

	fmt.Println(int(myAge) + yourAge)

}
