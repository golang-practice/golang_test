package main

import "fmt"

type Person struct {
	First string
	Last  string
	Age   int
}

type DoubleZero struct {
	Person
	First         string
	LicenseToKill bool
}

func main() {

	p1 := DoubleZero{
		Person: Person{
			First: "John",
			Last:  "Mccain",
			Age:   20},
		First: "Double Zero Seven", LicenseToKill: true}
	fmt.Println(p1.First, p1.LicenseToKill)
	fmt.Println(p1.Person.First, p1.Person.Last, p1.Person.Age)
}
