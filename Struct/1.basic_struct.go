package main

import "fmt"

func main() {

	type address struct {
		firstName string
		lastName  string
		Age       int
	}
	fmt.Println(address{"John", "Sposato", 35})
	p1 := address{"Gregory", "Woods", 10}
	fmt.Println(p1)
	fmt.Println(p1.firstName, p1.Age)

}
