package main

import "fmt"

func main() {

	mySlice := []string{"India", "China", "Germany", "USA"}

	fmt.Println(mySlice)
	mySlice = append(mySlice[:2], mySlice[3:]...)
	fmt.Println(mySlice)
}
