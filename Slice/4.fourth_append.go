package main

import "fmt"

func main() {

	mySlice := make([]string, 3, 4)
	mySlice[0] = "Good Guy"
	mySlice[1] = "Great Munda"
	mySlice[2] = "Greatest Gobbur"
	mySlice = append(mySlice, "New Guy")
	mySlice = append(mySlice, "Two Guy")
	mySlice = append(mySlice, "Three Guy")

	fmt.Println(mySlice)
	fmt.Println(mySlice[2])
	fmt.Println(mySlice[3])
	fmt.Println(mySlice[4])
	fmt.Println(mySlice[5])

}
