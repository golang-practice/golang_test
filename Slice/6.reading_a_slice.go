package main

import "fmt"

func main() {
	mySlice := []int{90, 60, 40, 50, 34, 49, 30}

	mySlice1 := mySlice[1:5] // [60 40 50 34]
	mySlice2 := mySlice[0:]  // [90 60 40 50 34 49 30]
	mySlice3 := mySlice[:6]  // [90 60 40 50 34 49]
	mySlice4 := mySlice[:]   // [90 60 40 50 34 49 30]
	mySlice5 := mySlice[2:4] // [40 50]

	fmt.Println(mySlice)  // [90 60 40 50 34 49 30]
	fmt.Println(mySlice1) // [60 40 50 34]
	fmt.Println(mySlice2) // [90 60 40 50 34 49 30]
	fmt.Println(mySlice3) // [90 60 40 50 34 49]
	fmt.Println(mySlice4) // [90 60 40 50 34 49 30]
	fmt.Println(mySlice5) // [40 50]
}
