package main

import "fmt"

func main() {

	var student []string
	fmt.Println(student)

	students := []string{}
	fmt.Println(students)

	studentss := make([]string, 1)
	fmt.Println(studentss)

	studentsss := make([]string, 3, 6)
	fmt.Println(studentsss)

	testSlice := []string{"God", "is", "Great"}
	fmt.Println("Length of this Slice is:", len(testSlice), "Capacity of this Slice is:", cap(testSlice))

}
