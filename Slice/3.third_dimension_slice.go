package main

import "fmt"

func main() {
	mySlice := make([]int, 0, 3)
	fmt.Print("------My First Slice------\n")
	fmt.Println(mySlice)
	fmt.Println("This slice length is", len(mySlice), "and its Capacity is", cap(mySlice))

	for i := 0; i < 80; i++ {
		mySlice = append(mySlice, i)
		fmt.Println("Len:", len(mySlice), "Capacity:", cap(mySlice))
	}
	fmt.Println(mySlice)

}
