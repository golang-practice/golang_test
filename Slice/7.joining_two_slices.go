package main

import "fmt"

func main() {
	mySlice := []string{"He", "is", "John"}
	mySliceNew := []string{"He", "stays", "here"}
	mySlice = append(mySlice, mySliceNew...)
	fmt.Print(mySlice)
}
