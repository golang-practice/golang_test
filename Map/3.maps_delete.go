package main

import "fmt"

func main() {

	mygreetings := map[string]int{
		"Why":   5,
		"What":  8,
		"Where": 55}
	fmt.Println(mygreetings)
	mygreetings["Whose"] = 85
	fmt.Println(mygreetings)
	delete(mygreetings, "What")
	fmt.Println(mygreetings)

	mygreetingss := map[int]string{
		1:  "Tryit",
		88: "Whento"}
	fmt.Println(mygreetingss)
	mygreetingss[5] = "Whyto"
	fmt.Println(mygreetingss)
	delete(mygreetingss, 1)
	fmt.Println(mygreetingss)

}
