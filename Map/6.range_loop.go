package main

import "fmt"

func main() {
	mygreeting := map[int]string{
		0: "Good Morning!",
		1: "Bonjour!",
		2: "Buenos dias!",
		3: "Bongiorno!"}
	fmt.Println(mygreeting)
	for key, value := range mygreeting {
		fmt.Println(key, "---", value)
	}
}
