// Different ways to write maps
package main

import "fmt"

func main() {
	var mygreeting1 map[string]int

	var mygreeting2 = make(map[string]int)
	mygreeting2["Great"] = 1

	var withVarMygreeting3 = map[string]int{
		"Great": 5,
		"Good":  8}

	mygreeting4 := map[string]int{}
	mygreeting4["TestingOn"] = 7

	mygreeting44 := make(map[string]int)
	mygreeting44["Testing"] = 5
	withoutVarMygreeting5 := map[string]int{"New": 55, "Try": 85}

	fmt.Println(mygreeting1)
	fmt.Println(mygreeting1 == nil)
	fmt.Println(mygreeting2)
	fmt.Println(withVarMygreeting3)
	fmt.Println(mygreeting4)
	fmt.Println(mygreeting44)
	fmt.Println(mygreeting44 == nil)
	fmt.Println(withoutVarMygreeting5)
}
