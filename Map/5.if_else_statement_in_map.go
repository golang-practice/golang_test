package main

import "fmt"

func main() {
	mygreeting := map[int]string{
		0: "Good Morning!",
		1: "Bonjour!",
		2: "Buenos dias!",
		3: "Bongiorno!"}

	fmt.Println(mygreeting)

	if value, exists := mygreeting[4]; exists {
		delete(mygreeting, 4)
		fmt.Println("Val: ", value)
		fmt.Println("Exists: ", exists)

	} else {
		fmt.Println("Value does not exist")
		fmt.Println("Val: ", value)
		fmt.Println("Exists: ", exists)

	}
	fmt.Println(mygreeting)

}
