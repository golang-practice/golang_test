package main

import "fmt"

func main() {

	mygreeting := map[string]int{}
	mygreeting["k1"] = 7
	mygreeting["k2"] = 13
	fmt.Println(mygreeting)
	delete(mygreeting, "k2")
	fmt.Println(mygreeting)

	v, ok := mygreeting["k1"]
	fmt.Println("It is:", ok, v)
}
